all:
	pandoc -t revealjs -s slides.md -o slides.html
	open slides.html

clean:
	rm -f slides.{html,pdf}


slides.pdf:
	pandoc -t beamer      slides.md -o slides.pdf -V 'theme:Warsaw'
	open slides.pdf
.PHONY: slides.pdf
