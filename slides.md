% KRR: Knowledge Representation & Reasoning
% Anderson Carlos, Christophe Escobar, Pierre Fenoll, Christophe Vermorel
% 2 avril 2014

## Summary

- **General Introduction**
    * What is Knowledge?
    * Looking for a Language
    * An application: Snow `is` White
- **Knowledge Representation Languages**
    * Knowledge Representation
    * Language Requirements
- **First-Order Logic (FOL)**
    * Knowledge base
    * Exemple in FOL
- **Frames Formalism Representation**
    * Frame Reasoning: inheritance
    * Frame querrying
- **Semantic Web**
- **Conclusion**
- **Bibliography**


# General Introduction

## KR & R
* How can knowledge be represented symbolically and manipulated in an automated way by reasoning programs?
    * **Knowledge**: some information about the world
        * medical information about some particular set of diseases: *what causes them*, *how to diagnose them*
        * geographical data: *which city is the capital of which country*, *population statistics*, …
        * common sense physics: *bodies can't go through solid walls*, …
    * **Representation**: *how* | *in which language* do we represent this information
    * **Reasoning**: how to extract more information from what is explicitly represented (because we can't represent every single fact as explicitly as in a database)

## What is Knowledge?
* Easier question: how do we talk about it?
    * We say “John **knows** that ‹*proposition*›”
    * which can be: true | false, right | wrong
* Contrast: “John **fears** that ...”: same content, different attitude
* Other forms of knowledge: know **how**, **who**, **what**, **when**, …
    * *Sensorimotor*: typing, riding a bicycle
    * *Affective*: deep understanding
    * *Belief*: not necessarily true and/or held for appropriate reasons
    * and *weaker yet*: “John **suspects** that …”
* Here: no distinction
    * the main idea = taking the world to be one way and not another

## It’s All about K
* Data **=** objective, quantifiable facts
* Information **=** data **+** meaning, with a context
* Knowledge **=** information **+** intrinsic value **(:** implications **|** connections**)**
    * eg.:
        * “lower temperature **means** need to order more fuel oil”
        * “high stock price **reflects** Bs’ value in that market sector”
* Addition of dimension of value **|** relationship comes from people
* People through experience **&** insight turn information into knowledge
* Knowledge **=** highest value, leads to action.
* Knowledge **=** the means **→** inform, **¬** a higher order of information
* Wisdom **=** optimally **|** effectively **|** efficiently apply knowledge, produce the desired results.

## Where to find K?
![ ](imgs/9.png)

## Nonaka's K Conversion
![ ](imgs/12.png)


## Looking for a Language
* **Mathematical definitions**: exact and complete
    * *triangle*: shape defined by three points that are not on a straight line and that are connected by lines
* **Natural kinds**: objects in the real word
    * *Definitions* of objects are often approximate and incomplete
    * Example:
        * ∀x(Human(x) → Animal(x))
        * ∀x(Human(x) → (walks(x) = upright ∧ … ∧ …))

## Formal KR
* **Logic** was originally developed as a language for mathematical reasoning
* Goal of Knowledge Representation different:
    * Represent **semantic** content of psychologically plausible memory models
* Need for semantics shared by most researchers
* In time, logic has become the *dominant* language (as probability theory for uncertainty reasoning)

## 1/2 Snow `is` White
* Two Prolog programs with identical behaviour:

~~~~~~~ {.prolog .numberLines}
printColour(snow) :- !, write("It’s white.").
printColour(grass) :- !, write("It’s green.").
printColour(sky) :- !, write("It’s yellow.").
printColour(X) :- !, write("Beats me.").
~~~~~~~

and

~~~~~~~ {.prolog .numberLines}
printColour(X) :- colour(X,Y), !, write("It’s "),
write(Y), write(".").
printColour(X) :- write("Beats me.").
colour(snow, white).
colour(sky, yellow).
colour(X,Z) : - madeof(X,Z), colour(Z,Y).
madeof(grass, vegetation).
colour(vegetation, green).
~~~~~~~

## 2/2 Snow `is` White
* Only the second program has explicit representation of ‘knowledge’ that snow is white
* the second program does what it does when asked for the colour of snow because of this knowledge.
When `colour(snow, white)` is removed, it will not print the right colour for snow.
* what makes the system knowledge-based **is not**
    * the use of a particular logical-looking language like Prolog
    * or having representation of true facts (`colour(sky, yellow)` is not)
    * or having lots of facts, or having a complex structure
* rather, it is *having explicit representation of knowledge which is used in the operation of the program*

# Knowledge Representation

## Language requirements

* Assumes the world contains
    * Objects: people, houses, numbers, colors, baseball games, wars, etc.
    * Relations: brother, bigger than, part of, comes between, etc.
    * Functions: father of, best friend, succ, etc.
* Expressiveness
* Inference
* Not only one, but a spectrum of languages
* Examples: FOL, DL, Prolog, Frame system formalism, etc.

## Knowledge base

* Collection of sentences given as the premises
    * Example: a finite set of sentences in the language of FOL
* The KB is the beliefs of the system that are *explicitly* given
* The entailments of the KB are the beliefs that are only *implicitly* given

# First-Order Logic

## Terms and Predicates

* **Terms** are simply names for **objects**.

* **Relations** are represented by a **predicate** applied to a
tuple of terms. A predicate represents a property of or
relation between terms that can be true or false.
    * Example:

```
Brother(John, Fred), Left-of(Square1, Square2)
GreaterThan(plus(1,1), plus(0,1))
```

## FOL for KR & R

* Representation: a set of sentences of FOL
* Reasoning: deducing logical consequences

## Syntax

* Connectives: ¬, ∧, ∨, ⇒ , ⇔, =
* Quanitﬁer: ∃ , ∀
* Constant: A , John , Car1
* Variable: x , y , z , etc.
* Predicate: Brother , Owns , etc.
* Function: Father-of , plus , etc.
* Example:
```
Owns(John,Car1) ∨ Owns(Fred, Car1)
Sold(John,Car1,Fred) ⇒ ¬Owns(John, Car1)
```


## Example in FOL

Three blocks are stacked:

```
+---+
| A |   <- Green
+---+
| B |
+---+
| C |   <- Non-green
+---+
```

Is there a green block directly on top of a non-green block?

## Formalization

```
S = {On(a,b), On(b,c), Green(a), ¬Green(c)}

α = ∃x∃y[Green(x) ∧ ¬Green(y) ∧ On(x,y)]
```

* Claim: S |= α
* Proof:
    * Let I be any interpretation such that I |= S.

    * Case 1:
```
  I |= Green(b)
∴ I |= Green(b) ∧ ¬Green(c) ∧ On(b,c)
∴ I |= α
```

    * Case 2:
```
  I |≠ Green(b)
∴ I |= ¬Green(b)
∴ I |= Green(a) ∧ ¬Green(b) ∧ On(a,b).
∴ I |= α
```

    * Either way, for any I, if I |= S then I |= α

So S |= α.

## Knowledge engineering in FOL

* Identify the task
* Assemble the relevant knowledge
* Decide on a vocabulary of predicates, functions, and constants
* Encode general knowledge about the domain
* Encode a description of the specific problem instance
* Pose queries to the inference procedure and get answers
* Debug the knowledge base

# Frame formalism representation

## Object-Oriented Representation

* FOL problem: knowledge about a given object could be scattered around the knowledge base
* As the KB grows, it became critical to organize sentences and procedures
* Think of knowledge itself structured and organized in terms of what the knowledge is : the objects

## Frame formalism

* Two types of frame:
    * individual frames to represent single objects
    * generic frames to represent categories or classes of objects
* Values stored in *slots*
* Item that go into slots are called *filler*
* Frame and slots name are atomic symbols
* Fillers are either atomic values or names of other individual frames
* constraints between slots are expressed by the attached and procedures :
    * IF-ADDED
    * IF-NEEDED

```
(Frame-name
    <:slot-name1 filler1>
    <:slot-name2 filler2> ...)
```

## Frame Reasoning : inheritance

* one declares that an object or situation exists instantiating some generic frame;
* any slot fillers that are not provided explicitly but can be inherited by the new
frame instance are inherited;
* any *IF-ADDED* procedure that can be inherited is run

## Frame querrying

* if there is a filler stored in the slot => return the value;
* otherwise, any *IF-NEEDED* procedure that can be inherited is run
* if there is no result : value unknown

## Examples with Frame formalism
```
(toronto
    <:INSTANCE-OF CanadianCity>
    <:Province ontario>
    <:Population 4.5M> ...)
(CanadianCity
    <:IS-A City>
    <:Province CanadianProvince>
    <:Country> canada> ...)
(City
    <:Population NonNegativeNumber> ...)
```
# Web X Internet

## Web

* Web 1.0
* Web 2.0
* Web 3.0

## Semantic Web : Definition

* "The Semantic Web is not a separate Web but an extension of the current one, in which information is given well-defined meaning, better enabling computers and people to work in cooperation."
* It is a source to retrieve information from the web (using the web spiders from RDF files) and access the data through Semantic Web Agents or Semantic Web Services.
* Source: "The Semantic Web" by Tim Berners-Lee, James Hendler, and Ora Lassila, Scientific American, 2001

## Semantic Web

![ ](imgs/semantic_web.png)

## Semantic Web : Challenges

* Vastness
* Vagueness
* Uncertainty
* Inconsistency
* Deceit

## Semantic Web : Projects

* DBpedia
* FOAF
* SIOC
* NextBio

# Conclusion

## Recap

* Knowledge representation and reasoning defines the very core of AI Logic, probability theory and decision theory form its theoretical foundations.
* The basis for building intelligent agents and applications.
* Concepts form the basis of modern theories on human knowledge representation and reasoning and their complexity.


# Bibliography

## Bibliography 1/2
* 1984 ~ A Fundamental Tradeoff in KRR ~ **Levesque**, **Brachman**
* 1985 ~ The Role of Frame-Based Representation in Reasoning ~ Fikes, Kehler
* 1992 ~ Task-Structure Analysis for Knowledge Modeling ~ Chandrasekaran, Johnson, Smith
* 1995 ~ First-Order Logic and Automated Theorem Proving ~ Melvin Fitting
* 1997 ~ Knowledge Engineering, Principles and methods ~ Studer, Benjamins, Fensel
* 2000 ~ KRR Logics for Artificial Intelligence ~ Shapiro
* 2000 ~ Context KRR in the Context Interchange System ~ Bressan, Goh, Levina, Madnick, Shah, Siegel
* 2001 ~ Knowledge representation, reasoning and declarative solving with Answer sets ~ Chitta

## Bibliography 2/2
* 2003 ~ KRR Book ~ **Brachman**
* 2003 ~ Conceptual KRR, PhD ~ Oldager
* 2005 ~ KRR, Slides for the Book ~ **Brachman**, **Levesque**
* 2006 ~ A Novel Genetic Fuzzy/Knowledge Petri Net Model and Its Applications ~ Zha
* 2009 ~ Aus DoD ~ Survey of KRR Systems ~ Trentelman
* 2012? ~ KRR on the Semantic Web with OWL ~ Horrocks, Patel-Schneider
* Book ~ Knowledge Acquisition, Representation, and Reasoning
* Knowledge representation and reasoning - Wikipedia
